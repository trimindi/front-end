import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SimpleLayoutComponent, FullLayoutComponent} from './container';
import {
  LoginFormComponent,
  DashboardComponent,
  AccountComponent,
  BukubesarComponent,
  AccountFormComponent,
  ShuComponent,
  UnitBisnisComponent,
  AnggotaDashboardComponent,
  AnggotaApprvalComponent,
  AnggotaMasterComponent,
  InstansiComponent,
  UspDashboardComponent,
  SimpananDashboardComponent,
  SimpananSettingComponent,
  SimpananSetoranComponent
} from './components';
import {CoaIsLoadedGuard, MasterIsLoadedGuard, IsAuthenticatedGuard} from './guards';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    component: LoginFormComponent
  },
  {
    path: 'dashboard',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        },
      }
    ]
  },
  {
    path: 'setting',
    component: FullLayoutComponent,
    data: {
      title: 'Setting'
    },
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        },
      }
    ]
  },
  {
    path: 'pengaturan',
    component: FullLayoutComponent,
    data: {
      title: 'Pengaturan'
    },
    canActivate: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    canActivateChild: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    children: [
      {
        path: 'account',
        component: AccountComponent,
        data: {
          title: 'Account'
        }
      },
      {
        path: 'instansi',
        component: InstansiComponent,
        data: {
          title: 'Instansi'
        }
      },
      {
        path: 'bukubesar',
        component: BukubesarComponent,
        data: {
          title: 'Bukubesar'
        }
      },
      {
        path: 'shu',
        component: ShuComponent,
        data: {
          title: 'Bukubesar'
        }
      },
      {
        path: 'unit',
        component: UnitBisnisComponent,
        data: {
          title: 'Bukubesar'
        }
      }
    ]
  },
  {
    path: 'anggota',
    component: FullLayoutComponent,
    canActivate: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    canActivateChild: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    data: {
      title: 'Setting'
    },
    children: [
      {
        path: 'dashboard',
        component: AnggotaDashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'master',
        component: AnggotaMasterComponent,
        data: {
          title: 'Master'
        }
      },
      {
        path: 'approval',
        component: AnggotaApprvalComponent,
        data: {
          title: 'Approval'
        }
      }
    ]
  },
  {
    path: 'simpanan',
    component: FullLayoutComponent,
    canActivate: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    canActivateChild: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    data: {
      title: 'Simpanan'
    },
    children: [
      {
        path: '',
        component: SimpananDashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'setting',
        component: SimpananSettingComponent,
        data: {
          title: 'Setting Parameter Simpanan'
        }
      },
      {
        path: 'setoran',
        component: SimpananSetoranComponent,
        data: {
          title: 'Setoran Simpanan'
        }
      },
      {path: '**', redirectTo: "simpanan"}
    ]
  },
  {
    path: 'usp',
    component: FullLayoutComponent,
    canActivate: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    canActivateChild: [CoaIsLoadedGuard, MasterIsLoadedGuard],
    data: {
      title: 'Unit Simpan Pinjam'
    },
    children: [
      {
        path: 'dashboard',
        component: UspDashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {path: '**', redirectTo: "dashboard"}
    ]
  },
  {path: '**', redirectTo: "/dashboard"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
