import {HttpClientModule} from '@angular/common/http';
import {DataTablesModule} from 'angular-datatables';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {reducers, metaReducers} from './store/reducers';
import {effects, CustomRouterStateSerializer} from './store';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import * as fromComponent from './components';
import * as fromContainer from './container';
import * as fromService from './services';
import * as fromGuards from './guards';
import * as fromIntreceptor from './intreceptor';
import * as fromPipe from './utils/pipe';
import * as fromDirective from './utils/directive';
@NgModule({
  declarations: [
    AppComponent,
    ...fromComponent.component,
    ...fromContainer.containers,
    ...fromPipe.pipes,
    ...fromDirective.directive
  ],
  imports: [
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    !environment.production ?
      StoreDevtoolsModule.instrument({
        maxAge: 25 // Retains last 25 states
      })
      : [],
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    EffectsModule.forRoot(effects),
    DataTablesModule
  ],
  providers: [{
    provide: RouterStateSerializer,
    useClass: CustomRouterStateSerializer
  }, ...fromService.services, ...fromGuards.guards, ...fromIntreceptor.intreceptor],
  bootstrap: [AppComponent]
})
export class AppModule {
}
