import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, ChangeDetectionStrategy, Input, Output , EventEmitter } from '@angular/core';
@Component({
    selector: 'app-anggota-dashboard',
    templateUrl: 'anggota-dashboard.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class AnggotaDashboardComponent implements OnInit {
    items = [
        {
            title: 'Master',
            path: '/anggota/master',
            image: 'assets/images/avatar/matthew.png',
            description: 'master anggota'
        },
        {
            title: 'Approval',
            path: '/usp',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        }
    ];
    constructor() {}
    ngOnInit() {
    }
}
