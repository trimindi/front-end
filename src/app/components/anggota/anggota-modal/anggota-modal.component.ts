
import { Component, Input ,Output ,EventEmitter, ChangeDetectionStrategy ,OnInit } from '@angular/core';
import { Anggota, Instansi,Golongan } from './../../../models'
import * as fromStore from './../../../store';
import {  Store} from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
declare var $: any;
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-anggota-modal',
  templateUrl: './anggota-modal.component.html'
})
export class AnggotaModalComponent implements OnInit {
    anggota$ : Observable<Anggota[]>;
    loading$ : Observable<boolean>;
    instansi$ : Observable<Instansi[]>;
    golongan$ : Observable<Golongan[]>;
    selectedGolongan : any = 'SEMUA';
    selectedInstansi : any = 'SEMUA';
    status : any = 'SEMUA';
    input: string;
    @Output() onSelectAnggota = new EventEmitter<any>();
    constructor(private store: Store<fromStore.RootState>) {
    }
    ngOnInit(){
      this.anggota$ = this.store.select(fromStore.getAllAnggota);
      this.loading$  = this.store.select(fromStore.getAnggotaIsLoading);
      this.instansi$  = this.store.select(fromStore.getAllInstansi);
      this.golongan$  = this.store.select(fromStore.getAllGolongan);
    }
    show(item){
      this.input = item;
      $('#anggota-modal').modal('show');
    }

    refresh(){
      this.anggota$ = this.store.select(fromStore.getAllAnggota)
      .pipe(
        map(x => x.filter(x => {
          if(this.selectedInstansi == 'SEMUA'){
            return true;
          }
          return x.KI == this.selectedInstansi.KI;
        })),
        map(x => x.filter(x => {
          if(this.selectedGolongan == 'SEMUA'){
            return true;
          }
          return x.GOLONGAN == this.selectedGolongan.GOLONGAN;
        })),
        map(x => x.filter(x => {
          if(this.status == 'SEMUA'){
            return true;
          }
          return x.ST == this.status;
        })),
      );
    }
    pilih(item){
      $('#anggota-modal').modal('hide');
      let output = {
        input : this.input,
        payload : item
      }
      this.onSelectAnggota.emit(output);
    }
  }
