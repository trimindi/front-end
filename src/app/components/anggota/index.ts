import { AnggotaApprvalComponent } from './anggota-approval/anggota-approval.component';
import { AnggotaDashboardComponent } from './anggota-dashboard/anggota-dashboard.component';
import { AnggotaModalComponent } from './anggota-modal/anggota-modal.component';
import * as fromMasterAnggota from './master';
export const anggotaComponent: any[] = [
  ...fromMasterAnggota.anggotaMasterComponent,
  AnggotaApprvalComponent,
  AnggotaDashboardComponent,
  AnggotaModalComponent
];



export * from './anggota-approval/anggota-approval.component';
export * from './anggota-dashboard/anggota-dashboard.component';
export * from './master';
export * from './anggota-modal/anggota-modal.component';
