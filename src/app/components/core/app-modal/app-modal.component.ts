
import { Component, Input ,Output ,EventEmitter, ChangeDetectionStrategy } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-modal',
  template: `<div class="ui basic modal">
  <div class="ui icon header">
    <i class="icon" [ngClass]="class"></i>
    {{ title }}
  </div>
  <div class="content">
    <p>{{ message }}</p>
  </div>
  <div class="actions">
    <div (click)="action(false)" class="ui red basic cancel inverted button">
      <i class="remove icon"></i>
      Tidak
    </div>
    <div (click)="action(true)" class="ui green ok inverted button">
      <i class="checkmark icon"></i>
      Iya
    </div>
  </div>
</div>`
})
export class AppModalComponent {
    @Input('class') class: string = 'archive';
    @Input('message') message: string = 'Your inbox is getting full, would you like us to enable automatic archiving of old messages?';
    @Input('title') title : string = 'Archive Old Messages';

    @Output('action') result = new EventEmitter<boolean>();
    constructor() {
    }

    trigger(){
      $('.ui.basic.modal').modal('show');
    }
    action(event){
      $('.ui.basic.modal').modal('hide');
      this.result.emit(event);
    }
  }
