import { AppBreadcrumpComponent } from './app-breadcrumb/app-breadcrumb.component';
import { AppModalComponent } from './app-modal/app-modal.component';

export const coreComponent: any[] = [
    AppBreadcrumpComponent,
    AppModalComponent
];
