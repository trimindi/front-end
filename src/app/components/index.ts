import * as fromCore      from './core';
import * as fromSystem    from './system';
import * as fromSetting   from './setting';
import * as fromAnggota   from './anggota';
import * as fromUsp  from './usp';

export const component : any[] = [
  ...fromCore.coreComponent,
  ...fromSystem.systemComponent,
  ...fromSetting.settingComponent,
  ...fromAnggota.anggotaComponent,
  ...fromUsp.uspComponent
];

export * from './system';
export * from './core';
export * from './setting';
export * from './anggota';
export * from './usp';
