import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, ChangeDetectionStrategy, Input, Output , EventEmitter } from '@angular/core';
import { Account, BukuBesar } from '../../../../models/index';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
    selector: 'app-account-form',
    templateUrl: 'account-form.component.html'
})

export class AccountFormComponent implements OnInit {
    @Input() loading: boolean;
    @Input() account: Account;
    @Input() bukubesar: BukuBesar[];
    @Input() selectedBukubesar: BukuBesar;

    @Output("simpan") simpanEvent = new EventEmitter<Account>();
    @Output("batal") batalEvent = new EventEmitter<any>();
    accountForm: FormGroup;

    constructor(private fb: FormBuilder) {}
    ngOnInit() {
        this.accountForm = this.fb.group({
            bukubesar: [ this.selectedBukubesar || '0', [Validators.required]],
            vbukubesar: [ this.account.ACCBB || '', [Validators.required, Validators.minLength(4)]],
            urut: [ this.urut(), [Validators.required, Validators.minLength(4)]],
            nama: [this.account.KETERANGAN || '', [Validators.required, Validators.minLength(4)]],
        });
    }
    urut(){
      if(this.account.ACC === undefined){
        return ""
      }else{
        return this.account.ACC.substr(4);
      }

    }
    getForm(name) {
        return this.accountForm.get(name) as FormControl;
    }
    get namaIsInvalid() {
        return this.getForm('nama').hasError('required')
               && this.getForm('nama').touched;
    }

    get urutIsInvalid() {
        return this.getForm('urut').hasError('required')
               && this.getForm('urut').touched;
    }

    onChangeBukubesar() {
        this.accountForm.setValue({
            ...this.accountForm.value,
            vbukubesar: this.accountForm.value.bukubesar.ACCBB
          });
    }

    simpan(){
      let form = this.accountForm.value;
      let data : Account = {
        ...this.account,
        ACC: this.account.ACCBB + form.urut,
        KETERANGAN: form.nama,
        GOLONGAN: this.account.GOLONGAN || 'USER'

      }
      this.simpanEvent.emit(data)
    }

    batal(){
      this.batalEvent.emit('');
    }
}
