
import { Component, Input ,Output ,EventEmitter, ChangeDetectionStrategy ,OnInit } from '@angular/core';
import { Jenis, Kelompok, BukuBesar, Account, UnitBisnis } from './../../../../models'
import * as fromStore from './../../../../store';
import {  Store} from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
declare var $: any;

@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html'
})
export class AccountModalComponent implements OnInit {
    unit$: Observable<UnitBisnis[]>;
    loading$ : Observable<boolean>;
    jenis$: Observable<Jenis[]>;
    kelompok$: Observable<Kelompok[]>;
    bukubesar$: Observable<BukuBesar[]>;
    account$: Observable<Account[]>;
    @Output('onSelectAccount') onSelectAccount = new EventEmitter<any>();
    selectedBukubesar : any = '0';
    selectedKelompok : any = '0';
    selectedJenis : any = '0';
    selectedUnitBisnis : any = '0';
    input : string;
    constructor(private store: Store<fromStore.RootState>) {
    }
    ngOnInit(){
      this.unit$ = this.store.select(fromStore.getActiveUnitBisnis);
      this.jenis$  = this.store.select(fromStore.getAllJenis);
      this.kelompok$  = this.store.select(fromStore.getKelompokFilteredJenis);
      this.bukubesar$  = this.store.select(fromStore.getBukubesarFilteredKelompok);
      this.account$  = this.store.select(fromStore.getAccountFilteredBukubesar);
      this.loading$ = this.store.select(fromStore.getAccountIsLoading);
    }
    show(item){
      this.input = item;
      $('#account-modal').modal('show');
    }

    onChangeUnitBisnis() {
        this.selectedJenis = '0';
        this.selectedKelompok = '0';
        this.selectedBukubesar = '0';
        this.store.dispatch(new fromStore.SelectUnitBisnis(this.selectedUnitBisnis));
    }

    onChangeJenis() {
        this.selectedKelompok = '0';
        this.selectedBukubesar = '0';
        this.store.dispatch(new fromStore.SelectJenisAccount(this.selectedJenis));
    }
    onChangeKelompok() {
        this.selectedBukubesar = '0';
        this.store.dispatch(new fromStore.SelectKelompokAccount(this.selectedKelompok));
    }

    onChangeBukubesar() {
        this.store.dispatch(new fromStore.SelectBukubesar(this.selectedBukubesar));
    }
    pilih(item){
      $('#account-modal').modal('hide');
      let output = {
        input : this.input,
        payload : item
      }
      this.onSelectAccount.emit(output);
    }
  }
