import { AccountComponent } from './account/account.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountFormComponent } from './account-form/account-form.component'
import { AccountModalComponent } from './account-modal/account-modal.component'

export const accountComponent: any[] = [
  AccountFormComponent,
  AccountListComponent,
  AccountComponent,
  AccountModalComponent
]

export * from './account/account.component';
export * from './account-list/account-list.component';
export * from './account-form/account-form.component'
export * from './account-modal/account-modal.component'
