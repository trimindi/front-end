import * as fromAccount from './account';
import * as fromBukubesar from './bukubesar';
import * as fromShu from './shu';
import * as fromUnitBisnis from './unit-bisnis';
import * as fromInstansi from './instansi';

export const settingComponent: any[] = [
  ...fromAccount.accountComponent,
  ...fromBukubesar.bukubesarComponent,
  ...fromShu.shuComponent,
  ...fromUnitBisnis.unitBisniComponent,
  ...fromInstansi.instansiComponent
];


export * from './account';
export * from './bukubesar';
export * from './shu';
export * from './unit-bisnis';
export * from './instansi';
