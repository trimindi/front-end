import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    items = [
        {
            title: 'Modul Master Anggota',
            path: '/anggota/dashboard',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul master anggota'
        },
        {
            title: 'Modul Unit Simpan Pinjam',
            path: '/usp',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Anggota'
        },
        {
            title: 'Modul Perdagangan',
            path: '/perdagangan',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Perdagangan'
        },
        {
            title: 'Modul Laporan',
            path: '/anggota',
            image: 'assets/images/avatar/matthew.png',
            description: 'Modul Laporan'
        }
    ];
    constructor() { }
    ngOnInit() { }
}
