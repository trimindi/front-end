import { LoginFormComponent } from './login-form/login-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';

export const systemComponent: any[] = [
    DashboardComponent,
    LoginFormComponent
];
export * from './login-form/login-form.component';
export * from './dashboard/dashboard.component';
