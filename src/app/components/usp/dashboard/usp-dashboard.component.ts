import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {  Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromStore from './../../../store';
import { UnitBisnis } from './../../../models';
import { Observable } from 'rxjs/Observable'
import { catchError, map,filter } from 'rxjs/operators';
@Component({
    selector: 'app-simpanan-dashboard-component',
    templateUrl: './usp-dashboard.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UspDashboardComponent implements OnInit {
    state = 'SIMPAN PINJAM';
    items$ : Observable<UnitBisnis[]>;
    constructor(private route : Router,private store: Store<fromStore.RootState>) { }
    ngOnInit() {
      this.items$ = this.store.select(fromStore.getAllUnitBisnis).pipe(
        map(x => x.filter(x => (x.JENIS == this.state) && (x.ST == '1')))
      )
    }
    pilih(item){
      this.state = item;
      this.items$ = this.store.select(fromStore.getAllUnitBisnis).pipe(
        map(x => x.filter(x => (x.JENIS == this.state) && (x.ST == '1')))
      )
    }
    select(item) {
      switch(item.JENIS){
        case 'SIMPAN PINJAM':{
          this.store.dispatch(new fromStore.Go({
            path: ['/simpanan']
          }));
          break;
        }
        case 'PERDAGANGAN':{
          this.store.dispatch(new fromStore.Go({
            path: ['/perdagangan']
          }));
          break;
        }
        case 'JASA': {
          this.store.dispatch(new fromStore.Go({
            path: ['/jasa']
          }));
          break;
        }
      }
    }
}
