import { UspDashboardComponent}  from './dashboard/usp-dashboard.component';
import * as fromSimpanan from './simpanan';
export const uspComponent : any[] = [
  UspDashboardComponent,
  ...fromSimpanan.simpananComponent
];

export * from './dashboard/usp-dashboard.component';
export * from './simpanan';
