import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-simpanan-dashboard-component',
    templateUrl: './simpanan-dashboard.component.html'
})
export class SimpananDashboardComponent implements OnInit {
    items = [
        {
            title: 'Setting',
            path: '/simpanan/setting',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Master',
            path: '/simpanan/master',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Setoran',
            path: '/simpanan/setoran',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Penarikan',
            path: '/simpanan/penarikan',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Pindah Buku',
            path: '/simpanan/pindah-buku',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Pendebetan Biaya Admin',
            path: '/simpanan/pendebetan-biaya-admin',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Penutupan',
            path: '/simpanan/penutupan',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },
        {
            title: 'Reverse / Pembatalan',
            path: '/simpanan/pembatalan',
            image: 'assets/images/avatar/matthew.png',
            description: '-'
        },

    ];
    constructor() { }
    ngOnInit() { }
}
