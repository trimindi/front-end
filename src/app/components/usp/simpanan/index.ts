import { SimpananDashboardComponent } from './dashboard/simpanan-dashboard.component';
import * as fromSetting from './setting';
import * as fromSetoran from './setoran';
export const simpananComponent : any[] = [
  SimpananDashboardComponent,
  ...fromSetting.simpananSettingComponent,
  ...fromSetoran.simpananSetoranComponent
]
export * from './dashboard/simpanan-dashboard.component';
export * from './setting';
export * from './setoran';
