import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JenisSimpanan, MasterSimpanan, Account, Anggota,Instansi } from './../../../../../models';
import { Store } from '@ngrx/store';
import * as fromStore from './../../../../../store';
@Component({
    selector: 'app-simpanan-setoran',
    templateUrl: './setoran-simpanan.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpananSetoranComponent implements OnInit {
    master$ : Observable<MasterSimpanan[]>;
    jenis$: Observable<any>;
    account$ :Observable<any>;
    anggota$ :Observable<any>;
    instansi$ :Observable<any>;
    constructor(private store : Store<fromStore.RootState>) {
      this.master$ = this.store.select(fromStore.getAllMasterSimpanan);
      this.jenis$ = this.store.select(fromStore.getJenisSimpananEntity);
      this.account$ = this.store.select(fromStore.getAccountEntity);
      this.anggota$ = this.store.select(fromStore.getAnggotaEntity);
      this.instansi$ = this.store.select(fromStore.getInstansiEntity);
    }
    ngOnInit() {

    }

}
