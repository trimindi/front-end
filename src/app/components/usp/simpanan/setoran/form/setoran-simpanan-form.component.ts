import { Component, OnInit, ChangeDetectionStrategy, ViewChild , Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseComponent } from './../../../../../utils';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  JenisSimpanan ,MasterSimpanan , Jenis ,Anggota ,Instansi, Account
} from './../../../../../models';

@Component({
    selector: 'app-simpanan-setoran-form',
    templateUrl: './setoran-simpanan-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpananSetoranFormComponent extends BaseComponent implements OnInit {
    @ViewChild('cariAnggotaModal') modal;
    jenis : JenisSimpanan;
    master: MasterSimpanan;
    @Input() account: any;
    @Input() jeniss : any;
    @Input() masters: MasterSimpanan[];
    @Input() anggota: any;
    @Input() instansi : any;
    acc : Account[];
    form : FormGroup;
    constructor(private fb: FormBuilder) {
      super();
      this.form = this.fb.group({
        CIB: [null,[Validators.required]],
        ACC: [null,[Validators.required]],
        KI: [null,[Validators.required]],
        NAMA_INSTANSI:[null,[Validators.required]],
        NAMA: [null,[Validators.required]],
        ID: [null,[Validators.required]],
        NO_ID: [null,[Validators.required]],
        TGLEXPIRED: [null,[Validators.required]],
        ALAMAT: [null,[Validators.required]],
        NOSIMP: [null,[Validators.required]],
      });

    }
    ngOnInit() {
      this.form.get('CIB').valueChanges.subscribe(r => {
        if(r !== null){
          this.master = undefined;
          let obj : Anggota = this.anggota[r];
          let patch = {
            KI: obj.KI,
            NAMA: obj.NAMA,
            ID: obj.ID,
            NO_ID: obj.NO_ID,
            TGLEXPIRED: obj.TGLEXPIRED,
            ALAMAT: obj.ALAMAT
          };
          this.form.patchValue(patch);
          this.jenis = this.jeniss['310101'];
          this.jenis = {
            ...this.jenis,
            KETERANGAN: this.account['310101'].KETERANGAN
          };
          this.master = this.masters.filter(r => {
            return r.CIB == obj.CIB && this.jenis.ACC == r.ACC;
          })[0];
          if(this.master !== undefined){
            let simp = {
              NOSIMP: this.master.NOSIMP
            };
            this.form.patchValue(simp);
          }else{
            window.alert("Anggota Tidak Memiliki simpanan Pokok");
          }


        }
      });
      this.form.get('KI').valueChanges.subscribe(r => {
        if(r !== null){
          let obj : Instansi = this.instansi[r];
          let patch = {
            NAMA_INSTANSI: obj.NAMA_INSTANSI
          };
          this.form.patchValue(patch);
        }
      });

    }
    onChangeJenisRekening(item){
      if(item.target.value !== ''){
        this.acc = Object.keys(this.account).map(id => this.account[id]).filter(x => {
          if(x.ACC.startsWith(item.target.value)){
            return x;
          }
          return false;
        });
      }
    }
    get jenissimp(){
      return this.jenis && this.jenis.ACC !== undefined ? this.jenis.ACC : null;
    }
    get jenissimpketerangan(){
      return this.jenis && this.jenis.KETERANGAN ? this.jenis.KETERANGAN : null;
    }

    cariAnggota(){
      this.modal.show('CIB');
    }
    getForm(){
      return this.form;
    }
    fromModal(event){
      let obj = {};
      obj[event.input] = event.payload.CIB;
      this.form.patchValue(obj)
    }

}
