import { Component, OnInit, ChangeDetectionStrategy, Input, Output , EventEmitter, ViewChild } from '@angular/core';
import { BaseComponent } from './../../../../../utils';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  JenisSimpanan
} from './../../../../../models';
@Component({
    selector: 'app-simpanan-setting-form',
    templateUrl: './simpanan-setting-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpananSettingFormComponent extends BaseComponent implements OnInit {
    @ViewChild('modal') modal;
    @Input() jenis: JenisSimpanan;
    @Input() account : any;
    @Input() loading : boolean;
    @Output('batal') batalEvent = new EventEmitter<any>()
    @Output('simpan') simpanEvent = new EventEmitter<any>()
    simpananSetting : FormGroup;
    constructor(private fb: FormBuilder) {
      super();
      this.simpananSetting = this.fb.group({
        ACC: ['',[Validators.required]],
        ACCKETERANGAN: ['',[]],
        BGA: ['',[Validators.required]],
        PJK: ['',[Validators.required]],
        SETAWAL: ['',[Validators.required]],
        SETMIN: ['',[Validators.required]],
        MINTARIK: ['',[Validators.required]],
        MAXTARIK: ['',[Validators.required]],
        SALMIN: ['',[Validators.required]],
        POT:['',[Validators.required]],
        SETORAN: ['',[Validators.required]],
        ADM: ['',[Validators.required]],
        ACCBGA: ['',[Validators.required]],
        ACCBGAKETERANGAN: ['',[Validators.required]],
        ACCPJK:['',[Validators.required]],
        ACCPJKKETERANGAN:['',[Validators.required]],
        ACCADM: ['',[Validators.required]],
        ACCADMKETERANGAN: ['',[Validators.required]],
        ACCPOT: ['',[Validators.required]],
        ACCPOTKETERANGAN: ['',[Validators.required]],
        PB: ['',[Validators.required]],
        ST: ['',[Validators.required]],
        KST: ['',[Validators.required]],
        KU:  ['',[Validators.required]],
        CIF: ['',[Validators.required]],
      });


    }
    change($event){
      console.log($event);
    }
    getForm(){
      return this.simpananSetting;
    }
    ngOnInit() {


      this.simpananSetting.get('ACC').valueChanges.subscribe(r => {
        if(r !== ''){
          let pay = {
            ACCKETERANGAN: this.account[r].KETERANGAN || '',
          };
          this.simpananSetting.patchValue(pay);
        }
      });
      this.simpananSetting.get('ACCBGA').valueChanges.subscribe(r => {
        if(r !== ''){
          let pay = {
            ACCBGAKETERANGAN: this.account[r].KETERANGAN || ''
          };
          this.simpananSetting.patchValue(pay);
        }

      });
      this.simpananSetting.get('ACCPJK').valueChanges.subscribe(r => {
        if(r !== ''){
          let pay = {
            ACCPJKKETERANGAN: this.account[r].KETERANGAN || '',
          };
          this.simpananSetting.patchValue(pay);
        }
      })
      this.simpananSetting.get('ACCADM').valueChanges.subscribe(r => {
        if(r !== ''){
          let pay = {
            ACCADMKETERANGAN: this.account[r].KETERANGAN || '',
          };
          this.simpananSetting.patchValue(pay);
        }
      });
      this.simpananSetting.get('ACCPOT').valueChanges.subscribe(r => {
        if(r !== ''){
          let pay = {
            ACCPOTKETERANGAN: this.account[r].KETERANGAN || '',
          };
          this.simpananSetting.patchValue(pay);
        }
      });
      this.simpananSetting.patchValue(this.jenis);
    }
    fromModal(item){
      const { input , payload } = item;
      let pay = {};
      pay[input] = payload.ACC;
      this.simpananSetting.patchValue(pay);
    }
    cariAccount(item){
      this.modal.show(item);
    }

    simpan(){

    }

    batal(){
      this.batalEvent.emit('');
    }
}
