import { of } from 'rxjs/observable/of';
import { filter, map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { JenisSimpanan } from './../../../../../models'
import { Component, OnInit, ChangeDetectionStrategy, Input, Output , EventEmitter } from '@angular/core';


@Component({
    selector: 'app-simpanan-setting-list',
    templateUrl: 'simpanan-setting-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class SimpananSettingListComponent implements OnInit {
    @Input('jenis') jenis: JenisSimpanan[];
    @Input('loading') loading: boolean;

    @Output('edit') editEvent = new EventEmitter<any>();
    @Output('delete') deleteEvent = new EventEmitter<any>();
    constructor() {}
    ngOnInit() {
    }
    edit(item){
      this.editEvent.emit(item);
    }

    delete(item){
      this.deleteEvent.emit(item);
    }
}
