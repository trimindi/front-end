import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { JenisSimpanan } from './../../../../models';
import { Store } from '@ngrx/store';
import * as fromStore from './../../../../store';
@Component({
    selector: 'app-simpanan-setting',
    templateUrl: './simpanan-setting.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpananSettingComponent implements OnInit {
    jenis$ : Observable<JenisSimpanan[]>;
    loading$ : Observable<boolean>;
    selectedJenis$ : Observable<JenisSimpanan>;
    account$ : Observable<any>;
    constructor(private store : Store<fromStore.RootState>) {
      this.jenis$ = this.store.select(fromStore.getAllJenisSimpanan);
      this.loading$ = this.store.select(fromStore.getJenisSimpananIsLoading);
      this.selectedJenis$ = this.store.select(fromStore.getSelectedJenisSimpanan);
      this.account$ = this.store.select(fromStore.getAccountEntity);
    }
    ngOnInit() {

    }

    onListEdit(item){
      this.store.dispatch(new fromStore.JenisSimpananSelect(item));
    }

    onListDelete(item){

    }
    onFormBatal(){
      this.store.dispatch(new fromStore.JenisSimpananSelect(null));
    }

}
