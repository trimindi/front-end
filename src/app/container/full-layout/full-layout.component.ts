import { Store } from '@ngrx/store';
import * as fromStore from './../../store';
import { Component,OnInit } from '@angular/core';
declare var jquery:any;
declare var $ :any;
import { routerTransition } from './../../utils'
@Component({
  selector: 'app-full-layout',
  animations: [ routerTransition ],
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.css']
})
export class FullLayoutComponent implements OnInit {
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
  constructor(private store : Store<fromStore.RootState>) {

  }
  ngOnInit(){
      $('.ui.menu .ui.dropdown').dropdown({
        on: 'hover'
      });
      $('.ui.menu a.item').on('click', function() {
          $(this)
            .addClass('active')
            .siblings()
            .removeClass('active')
          ;
        });
  }
  back() {
  }

  home(){
    this.store.dispatch(new fromStore.Go({
      path: ["/dashboard"]
    }));
  }
  logout() {
    localStorage.clear();
    this.store.dispatch(new fromStore.PerformLogout());
  }
 }
