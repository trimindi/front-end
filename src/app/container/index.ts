import { SimpleLayoutComponent } from './simple-layout/simple-layout.component';
import { FullLayoutComponent } from './full-layout/full-layout.component';
export * from './full-layout';
export * from './simple-layout';

export const containers: any[] = [
    FullLayoutComponent,
    SimpleLayoutComponent
];