import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { Observable } from 'rxjs/Observable';
import { tap, map, filter, take, switchMap } from 'rxjs/operators';
import * as fromRoot from './../../store';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate, CanActivateChild {
  constructor(private store: Store<fromRoot.RootState>) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return forkJoin([
      this.checkToken() ,
      this.checkIsLoggedIn()
    ]).pipe(
      map(x => {
        if(x[0] === false){
          this.store.dispatch(new fromRoot.Go({
            path: [ '/auth' ]
          }));
        }
        return x[0];
      })
    );
  }
  canActivateChild(
    route: ActivatedRouteSnapshot
  ): Observable<boolean> {
    return forkJoin([
      this.checkToken() ,
      this.checkIsLoggedIn()
    ]).pipe(
      map(x => {
        if(x[0] === false){
          this.store.dispatch(new fromRoot.Go({
            path: ['/auth']
          }));
        }
        return x[0];
      })
    );
  }
  checkIsLoggedIn(): Observable<boolean> {
    return this.store.select(fromRoot.getLoginIsLogedIn).pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromRoot.Go({
            path : ['/auth']
          }));
        }
      }),
      filter(loaded => loaded),
      take(1)
    );
  }
  checkToken(): Observable<boolean> {
    return Observable.create(true);
  }
}
