import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = {
      token: ""
    };
    if(localStorage.getItem('session') !== null){
      token = JSON.parse(localStorage.getItem('session'));
    }
    const authReq = req.clone({
      headers: req.headers.set('Authorization', token.token || "")
    });
    return next.handle(authReq).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      console.log('in intreceptor error');
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          console.log("Unathorization request");
        }
      }
    });
  }
}
