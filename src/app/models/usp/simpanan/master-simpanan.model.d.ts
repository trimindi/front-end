export interface MasterSimpanan {
  NOSIMP?: string;
  ACC?: string;
  CIB?: string;
  DEBET?: number;
  KREDIT?: number;
  BLOKIR?: number;
  SALDO?: number;
  TGLBUKA?: string;
  TGLAKTIF?: string;
  TGLBEBAN?: string;
  TGLSETOR?: string;
  TGLSETORSBL?: string;
  ST?: number;
  KU?: string;
  CIF?: string;
}
