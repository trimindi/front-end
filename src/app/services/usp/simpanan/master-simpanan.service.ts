import { IResponse, MasterSimpanan  } from './../../../models';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

@Injectable()
export class MasterSimpananService {
  constructor(private http: HttpClient) {
  }

  getMasterSimpanan(): Observable<MasterSimpanan[]> {
    return this.http
      .get<IResponse<MasterSimpanan[]>>(`/emikro/v1/simpanan/detail`)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  createMasterSimpanan(data : MasterSimpanan): Observable<MasterSimpanan> {
    return this.http
      .post<IResponse<MasterSimpanan>>(`/emikro/v1/simpanan/insert`,data)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
  updateMasterSimpanan(data : MasterSimpanan): Observable<MasterSimpanan> {
    return this.http
      .put<IResponse<MasterSimpanan>>(`/emikro/v1/simpanan/ubah`,data)
      .pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }

  deleteMasterSimpanan(data : MasterSimpanan): Observable<MasterSimpanan> {
    return this.http
      .request<IResponse<MasterSimpanan>>('DELETE',`/emikro/v1/simpanan/hapus`,{
        body: data
      }).pipe(
        map(r => r.data),
        catchError((error: any) => Observable.throw(error.json()))
      );
  }
}
