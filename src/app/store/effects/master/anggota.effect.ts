import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../../../store';

import { AnggotaActionType } from './../../actions';
import * as anggotaAction from './../../actions/master/anggota.action';
import * as fromServices from './../../../services';

@Injectable()
export class AnggotaEffect {
  constructor(
    private actions$: Actions,
    private service: fromServices.AnggotaService
  ) {}

  @Effect()
  loadAnggota$ = this.actions$.ofType(AnggotaActionType.ANGGOTA_LOAD).pipe(
    switchMap(() => {
      return this.service
        .getAnggota()
        .pipe(
          map(data => new anggotaAction.AnggotaLoadSuccess(data)),
          catchError(error => of(new anggotaAction.AnggotaLoadFail(error)))
        );
    })
  );

  @Effect()
  addAnggota$ = this.actions$.ofType(AnggotaActionType.ANGGOTA_ADD).pipe(
    map((action: anggotaAction.AnggotaAdd) => action.payload),
    switchMap(bb => {
      return this.service
        .createAnggota(bb)
        .pipe(
          map(acc => new anggotaAction.AnggotaAddSuccess(acc)),
          catchError(error => of(new anggotaAction.AnggotaAddFail(error)))
        );
    })
  );

  @Effect()
  updateAnggota$ = this.actions$.ofType(AnggotaActionType.ANGGOTA_UPDATE).pipe(
    map((action: anggotaAction.AnggotaUpdate) => action.payload),
    switchMap(bb => {
      return this.service
        .updateAnggota(bb)
        .pipe(
          map(acc => new anggotaAction.AnggotaUpdateSuccess(bb)),
          catchError(error => of(new anggotaAction.AnggotaUpdateFail(error)))
        );
    })
  );

  @Effect()
  deleteAnggota$ = this.actions$.ofType(AnggotaActionType.ANGGOTA_DELETE).pipe(
    map((action: anggotaAction.AnggotaDelete) => action.payload),
    switchMap(bb => {
      return this.service
        .deleteAnggota(bb)
        .pipe(
          map(acc => new anggotaAction.AnggotaDeleteSuccess(bb)),
          catchError(error => of(new anggotaAction.AnggotaDeleteFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      AnggotaActionType.ANGGOTA_ADD_SUCCESS,
      AnggotaActionType.ANGGOTA_UPDATE_SUCCESS
    ).pipe(
      map(bb => {
        return new fromRoot.Go({
          path: ['/anggota/master'],
        });
      })
    );
}
