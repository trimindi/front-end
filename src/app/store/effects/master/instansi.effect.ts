import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as fromRoot from './../../../store';
import { InstansiActionType } from './../../actions';
import * as instansiAction from './../../actions/master/instansi.action';
import * as fromServices from './../../../services';

@Injectable()
export class InstansiEffect {
  constructor(
    private actions$: Actions,
    private service: fromServices.InstansiService
  ) {}

  @Effect()
  loadInstansi$ = this.actions$.ofType(InstansiActionType.INSTANSI_LOAD).pipe(
    switchMap(() => {
      return this.service
        .getInstansi()
        .pipe(
          map(data => new instansiAction.InstansiLoadSuccess(data)),
          catchError(error => of(new instansiAction.InstansiLoadFail(error)))
        );
    })
  );

  @Effect()
  addInstansi$ = this.actions$.ofType(InstansiActionType.INSTANSI_ADD).pipe(
    map((action: instansiAction.InstansiAdd) => action.payload),
    switchMap(bb => {
      return this.service
        .createInstansi(bb)
        .pipe(
          map(acc => new instansiAction.InstansiAddSuccess(acc)),
          catchError(error => of(new instansiAction.InstansiAddFail(error)))
        );
    })
  );

  @Effect()
  updateInstansi$ = this.actions$.ofType(InstansiActionType.INSTANSI_UPDATE).pipe(
    map((action: instansiAction.InstansiUpdate) => action.payload),
    switchMap(bb => {
      return this.service
        .updateInstansi(bb)
        .pipe(
          map(acc => new instansiAction.InstansiUpdateSuccess(bb)),
          catchError(error => of(new instansiAction.InstansiUpdateFail(error)))
        );
    })
  );

  @Effect()
  deleteInstansi$ = this.actions$.ofType(InstansiActionType.INSTANSI_DELETE).pipe(
    map((action: instansiAction.InstansiDelete) => action.payload),
    switchMap(bb => {
      return this.service
        .deleteInstansi(bb)
        .pipe(
          map(acc => new instansiAction.InstansiDeleteSuccess(bb)),
          catchError(error => of(new instansiAction.InstansiDeleteFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      InstansiActionType.INSTANSI_ADD_SUCCESS,
      InstansiActionType.INSTANSI_UPDATE_SUCCESS
    ).pipe(
      map(bb => {
        return new fromRoot.Go({
          path: ['/pengaturan/instansi'],
        });
      })
    );
}
