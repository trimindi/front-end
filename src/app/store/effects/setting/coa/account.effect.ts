import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../../../../store';

import { AccountActionType } from './../../../actions';
import * as accountAction from './../../../actions/setting/coa/account.action';
import * as fromServices from './../../../../services';


@Injectable()
export class AccountEffect {
  constructor(
    private actions$: Actions,
    private accountService: fromServices.AccountService
  ) {}

  @Effect()
  loadAccount$ = this.actions$.ofType(AccountActionType.LOAD_ACCOUNT).pipe(
    switchMap(() => {
      return this.accountService
        .getAccount()
        .pipe(
          map(account => new accountAction.LoadAccountSuccess(account)),
          catchError(error => of(new accountAction.LoadAccountFail(error)))
        );
    })
  );
  @Effect()
  addAccount$ = this.actions$.ofType(AccountActionType.ADD_ACCOUNT).pipe(
    map((action: accountAction.AddAccount) => action.payload),
    switchMap(account => {
      return this.accountService
        .createAccount(account)
        .pipe(
          map(acc => new accountAction.AddAccountSuccess(account)),
          catchError(error => of(new accountAction.AddAccountFail(error)))
        );
    })
  );
  @Effect()
  updateAccount$ = this.actions$.ofType(AccountActionType.UPDATE_ACCOUNT).pipe(
    map((action: accountAction.UpdateAccount) => action.payload),
    switchMap(bb => {
      return this.accountService
        .updateAccount(bb)
        .pipe(
          map(acc => new accountAction.UpdateAccountSuccess(bb)),
          catchError(error => of(new accountAction.UpdateAccountFail(error)))
        );
    })
  );
  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      AccountActionType.ADD_ACCOUNT_SUCCESS,
      AccountActionType.UPDATE_ACCOUNT_SUCCESS
    ).pipe(
      map(account => {
        return new fromRoot.Go({
          path: ['/account/account'],
        });
      })
    );
}
