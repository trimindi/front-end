import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as fromRoot from './../../../../store';

import { BukuBesarActionType } from './../../../actions';
import * as bukubesarAction from './../../../actions/setting/coa/bukubesar.action';
import * as fromServices from './../../../../services';

@Injectable()
export class BukubesarEffect {
  constructor(
    private actions$: Actions,
    private bukubesarService: fromServices.BukubesarService
  ) {}

  @Effect()
  loadBukubesar$ = this.actions$.ofType(BukuBesarActionType.LOAD_BUKUBESAR).pipe(
    switchMap(() => {
      return this.bukubesarService
        .getBukabesar()
        .pipe(
          map(bukubesar => new bukubesarAction.LoadBukuBesarSuccess(bukubesar)),
          catchError(error => of(new bukubesarAction.LoadBukuBesarFail(error)))
        );
    })
  );

  @Effect()
  addBukubesar$ = this.actions$.ofType(BukuBesarActionType.ADD_BUKUBESAR).pipe(
    map((action: bukubesarAction.AddBukuBesar) => action.payload),
    switchMap(bb => {
      return this.bukubesarService
        .createBukubesar(bb)
        .pipe(
          map(acc => new bukubesarAction.AddBukuBesarSuccess(acc)),
          catchError(error => of(new bukubesarAction.AddBukuBesarFail(error)))
        );
    })
  );

  @Effect()
  updateBukubesar$ = this.actions$.ofType(BukuBesarActionType.UPDATE_BUKUBESAR).pipe(
    map((action: bukubesarAction.UpdateBukuBesar) => action.payload),
    switchMap(bb => {
      return this.bukubesarService
        .updateBukubesar(bb)
        .pipe(
          map(acc => new bukubesarAction.UpdateBukuBesarSuccess(bb)),
          catchError(error => of(new bukubesarAction.UpdateBukuBesarFail(error)))
        );
    })
  );

  @Effect()
  deleteBukubesar$ = this.actions$.ofType(BukuBesarActionType.DELETE_BUKUBESAR).pipe(
    map((action: bukubesarAction.DeleteBukuBesar) => action.payload),
    switchMap(bb => {
      return this.bukubesarService
        .removeBukubesar(bb)
        .pipe(
          map(acc => new bukubesarAction.DeleteBukuBesarSuccess(bb)),
          catchError(error => of(new bukubesarAction.DeleteBukuBesarFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      BukuBesarActionType.ADD_BUKUBESAR_SUCCESS,
      BukuBesarActionType.UPDATE_BUKUBESAR_SUCCESS
    ).pipe(
      map((action: bukubesarAction.AddBukuBesarSuccess) => action.payload),
      map(bb => {
        return new fromRoot.Go({
          path: ['/account/bukubesar'],
        });
      })
    );
}
