import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import { ShuActionType } from './../../../store';
import * as shuAction from './../../../store/actions/system/shu.action';
import * as fromServices from './../../../services';

@Injectable()
export class ShuEffect {
  constructor(
    private actions$: Actions,
    private shuService: fromServices.ShuService
  ) {}

  @Effect()
  loadShu$ = this.actions$.ofType(ShuActionType.LOAD_SHU).pipe(
    switchMap(() => {
      return this.shuService
        .getShu()
        .pipe(
          map(data => new shuAction.LoadShuSuccess(data)),
          catchError(error => of(new shuAction.LoadShuFail(error)))
        );
    })
  );

  @Effect()
  updateShu$ = this.actions$.ofType(ShuActionType.UPDATE_SHU).pipe(
    map((action: shuAction.UpdateShu) => action.payload),
    switchMap(shu => {
      return this.shuService
        .updateShu(shu)
        .pipe(
          map(data => new shuAction.UpdateShuSuccess(shu)),
          catchError(error => of(new shuAction.UpdateShuFail(error)))
        );
    })
  );
}
