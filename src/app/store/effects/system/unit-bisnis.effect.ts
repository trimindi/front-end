import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';

import { UnitBisnisActionType } from './../../../store';
import * as unitBisnis from './../../../store/actions';
import * as fromServices from './../../../services';

@Injectable()
export class UnitBisnisEffect {
  constructor(
    private actions$: Actions,
    private unitBisnisService: fromServices.UnitBisnisService
  ) {}

  @Effect()
  loadUnitBisnis$ = this.actions$.ofType(UnitBisnisActionType.LOAD_UNIT_BISNIS).pipe(
    switchMap(() => {
      return this.unitBisnisService
        .getUnitBisnis()
        .pipe(
          map(data => new unitBisnis.LoadUnitBisnisSuccess(data)),
          catchError(error => of(new unitBisnis.LoadUnitBisnisFail(error)))
        );
    })
  );

  @Effect()
  updateUnitBisnis$ = this.actions$.ofType(UnitBisnisActionType.UPDATE_UNIT_BISNIS).pipe(
    map((action: unitBisnis.UpdateUnitBisnis) => action.payload),
    switchMap(data => {
      return this.unitBisnisService
        .updateUnitBisnis(data)
        .pipe(
          map(data => new unitBisnis.UpdateUnitBisnisSuccess(data)),
          catchError(error => of(new unitBisnis.UpdateUnitBisnisFail(error)))
        );
    })
  );
}
