import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as fromRoot from './../../../../store';
import { JenisSimpananActionType } from './../../../actions';
import * as instansiAction from './../../../actions/usp/simpanan/jenis-simpanan.action';
import * as fromServices from './../../../../services';

@Injectable()
export class JenisSimpananEffect {
  constructor(
    private actions$: Actions,
    private service: fromServices.JenisSimpananService
  ) {}

  @Effect()
  loadJenisSimpanan$ = this.actions$.ofType(JenisSimpananActionType.JENIS_SIMPANAN_LOAD).pipe(
    switchMap(() => {
      return this.service
        .getJenisSimpanan()
        .pipe(
          map(data => new instansiAction.JenisSimpananLoadSuccess(data)),
          catchError(error => of(new instansiAction.JenisSimpananLoadFail(error)))
        );
    })
  );

  @Effect()
  addJenisSimpanan$ = this.actions$.ofType(JenisSimpananActionType.JENIS_SIMPANAN_ADD).pipe(
    map((action: instansiAction.JenisSimpananAdd) => action.payload),
    switchMap(bb => {
      return this.service
        .createJenisSimpanan(bb)
        .pipe(
          map(acc => new instansiAction.JenisSimpananAddSuccess(acc)),
          catchError(error => of(new instansiAction.JenisSimpananAddFail(error)))
        );
    })
  );

  @Effect()
  updateJenisSimpanan$ = this.actions$.ofType(JenisSimpananActionType.JENIS_SIMPANAN_UPDATE).pipe(
    map((action: instansiAction.JenisSimpananUpdate) => action.payload),
    switchMap(bb => {
      return this.service
        .updateJenisSimpanan(bb)
        .pipe(
          map(acc => new instansiAction.JenisSimpananUpdateSuccess(bb)),
          catchError(error => of(new instansiAction.JenisSimpananUpdateFail(error)))
        );
    })
  );

  @Effect()
  deleteJenisSimpanan$ = this.actions$.ofType(JenisSimpananActionType.JENIS_SIMPANAN_DELETE).pipe(
    map((action: instansiAction.JenisSimpananDelete) => action.payload),
    switchMap(bb => {
      return this.service
        .deleteJenisSimpanan(bb)
        .pipe(
          map(acc => new instansiAction.JenisSimpananDeleteSuccess(bb)),
          catchError(error => of(new instansiAction.JenisSimpananDeleteFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      JenisSimpananActionType.JENIS_SIMPANAN_ADD_SUCCESS,
      JenisSimpananActionType.JENIS_SIMPANAN_UPDATE_SUCCESS
    ).pipe(
      map(bb => {
        return new fromRoot.Go({
          path: ['/simpanan/setting'],
        });
      })
    );
}
