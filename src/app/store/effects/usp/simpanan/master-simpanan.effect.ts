import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { map, switchMap, catchError } from 'rxjs/operators';
import * as fromRoot from './../../../../store';
import { MasterSimpananActionType } from './../../../actions';
import * as masterAction from './../../../actions/usp/simpanan/master-simpanan.action';
import * as fromServices from './../../../../services';

@Injectable()
export class MasterSimpananEffect {
  constructor(
    private actions$: Actions,
    private service: fromServices.MasterSimpananService
  ) {}

  @Effect()
  loadMasterSimpanan$ = this.actions$.ofType(MasterSimpananActionType.MASTER_SIMPANAN_LOAD).pipe(
    switchMap(() => {
      return this.service
        .getMasterSimpanan()
        .pipe(
          map(data => new masterAction.MasterSimpananLoadSuccess(data)),
          catchError(error => of(new masterAction.MasterSimpananLoadFail(error)))
        );
    })
  );

  @Effect()
  addMasterSimpanan$ = this.actions$.ofType(MasterSimpananActionType.MASTER_SIMPANAN_ADD).pipe(
    map((action: masterAction.MasterSimpananAdd) => action.payload),
    switchMap(bb => {
      return this.service
        .createMasterSimpanan(bb)
        .pipe(
          map(acc => new masterAction.MasterSimpananAddSuccess(acc)),
          catchError(error => of(new masterAction.MasterSimpananAddFail(error)))
        );
    })
  );

  @Effect()
  updateMasterSimpanan$ = this.actions$.ofType(MasterSimpananActionType.MASTER_SIMPANAN_UPDATE).pipe(
    map((action: masterAction.MasterSimpananUpdate) => action.payload),
    switchMap(bb => {
      return this.service
        .updateMasterSimpanan(bb)
        .pipe(
          map(acc => new masterAction.MasterSimpananUpdateSuccess(bb)),
          catchError(error => of(new masterAction.MasterSimpananUpdateFail(error)))
        );
    })
  );

  @Effect()
  deleteMasterSimpanan$ = this.actions$.ofType(MasterSimpananActionType.MASTER_SIMPANAN_DELETE).pipe(
    map((action: masterAction.MasterSimpananDelete) => action.payload),
    switchMap(bb => {
      return this.service
        .deleteMasterSimpanan(bb)
        .pipe(
          map(acc => new masterAction.MasterSimpananDeleteSuccess(bb)),
          catchError(error => of(new masterAction.MasterSimpananDeleteFail(error)))
        );
    })
  );

  @Effect()
  createAccountSuccess$ = this.actions$
    .ofType(
      MasterSimpananActionType.MASTER_SIMPANAN_ADD_SUCCESS,
      MasterSimpananActionType.MASTER_SIMPANAN_UPDATE_SUCCESS
    ).pipe(
      map(bb => {
        return new fromRoot.Go({
          path: ['/simpanan/master'],
        });
      })
    );
}
