export const ApiUrl = {
  system: {
    login: {
      auth: "",
      refresh: ""
    },
    shu: {}
  },
  master: {
    pendidikan: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    golongan: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    identitas: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    pekerjaan: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    agama: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    jabatan: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    hubungan: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    }
  },
  coa: {
    account: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    bukubesar: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    jenis: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
    kelompok: {
      base: "",
      insert: "",
      update: "",
      delete: "",
    },
  }
}
