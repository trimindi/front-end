import { FormGroup, FormControl } from '@angular/forms';
export abstract class BaseComponent {
    abstract getForm() : FormGroup

    isRequired(name){
      let form : FormControl = this.getForm().get(name) as FormControl;
      return form.hasError('required') && form.touched;
    }

    isError(name,type){
      let form : FormControl = this.getForm().get(name) as FormControl;
      return form.hasError(type) && form.touched;
    }
}
